set rtp+=/home/tinix/anaconda3/lib/python3.7/site-packages/powerline/bindings/vim/

let g:airline_theme='molokai' 
"mas info en .vim/bundle/vim-airline-theme/autoload/airline/theme
set laststatus=2
set t_Co=256

set showtabline=1
set noshowmode
set autoindent
set cindent
set number
"set colorcolumn=110
" line roja costado derecho :(
set tabstop=8 "ser for C programming"
colo default

syntax on
set expandtab
set tabstop=7
set mouse=a
set ruler
set cmdheight=2
set pastetoggle=<F9> "copiar y pegar
set nobackup
set noswapfile

execute pathogen#infect()

set nocompatible " Use vim , right?

"For vundle
filetype off
set rtp+=~/.vim/bundle/vundle
call vundle#rc()

" Dependency of snipmate, not sure what it is :/
Bundle "MarcWeber/vim-addon-mw-utils"
" Utility Funtions for other plugins, snipmate dependency
Bundle "tomtom/tlib_vim"
" Collection of snippet , snippet dependency
Bundle "honza/vim-snippets"
" Snipperts ofr our use :)
Bundle 'garbas/vim-snipmate'
" Good looking bottom :)
"Bundle 'bling/vim-airline'
"  Git tools
Bundle 'tpope/vim-fugitive'
" Dependnecy managment
Bundle 'gmarik/vundle'
" Rails :/
Bundle 'tpope/vim-rails.git'
"Commenting and uncommenting stuf
Bundle 'tomtom/tcomment_vim' 
" Molokai theme
Bundle 'vim-ruby/vim-ruby'
" Surround your code :)
Bundle 'tpope/vim-surround'
"Parentesis autogeneraso
Bundle 'jiangmiao/auto-pairs'
" Genera blokes de codido comunes tab completations
 Bundle 'ervandew/supertab'
"Fuzzy finder for vim (Ctrl + P)
Bundle 'kien/ctrlp.vim'
"Distpatching the test runner to tmux pane
Bundle 'tpope/vim-dispatch'
"NERDTreeToogle
Bundle  'scrooloose/nerdtree'
" esperimental vundle
Bundle 'tpope/vim-vinegar'


Bundle 'airblade/vim-gitgutter'
Bundle 'cakebaker/scss-syntax.vim'
Bundle 'craigemery/vim-autotag'
Bundle 'chriskempson/base16-vim'
Bundle 'danro/rename.vim'
Bundle 'edkolev/tmuxline.vim'
Bundle 'heartsentwined/vim-emblem'
Bundle 'kchmck/vim-coffee-script'
Bundle 'michaeljsmith/vim-indent-object'
Bundle 'rking/ag.vim'
Bundle 'sophacles/vim-bundle-mako'
Bundle 'terryma/vim-expand-region'
Bundle 'thoughtbot/vim-rspec'
Bundle 'tpope/vim-bundler'
Bundle 'tpope/vim-commentary'
Bundle 'tpope/vim-endwise'
Bundle 'tpope/vim-repeat'
Bundle 'tpope/vim-speeddating'
Bundle 'slim-template/vim-slim'
Bundle 'scrooloose/syntastic'
Bundle 'ngmy/vim-rubocop'
Bundle 'mustache/vim-mustache-handlebars'
Bundle 'vim-scripts/indenthtml.vim'
Bundle 'camthompson/vim-ember'
Bundle 'tpope/vim-unimpaired'
Bundle 'tpope/vim-rhubarb'
Bundle 'SirVer/ultisnips'
Bundle 'dhruvasagar/vim-table-mode'
Bundle 'godlygeek/tabular'
Bundle 'plasticboy/vim-markdown'
Bundle 'dyng/ctrlsf.vim' 
"for really nice search results like in sublime
Bundle 'yonchu/accelerated-smooth-scroll'
Bundle 'vim-scripts/IndexedSearch'
Bundle 'itspriddle/vim-jquery'
Bundle 'tpope/vim-haml'
Bundle 'junegunn/goyo.vim'

"Ruby stuff: Thanks Ben :)
" ================
syntax on                 " Enable syntax highlighting  
filetype plugin indent on " Enable filetype-specific indenting and plugins

augroup myfiletypes  
    " Clear old autocmds in group
    autocmd!
    " autoindent with two spaces, always expand tabs
    autocmd FileType ruby,eruby,yaml,markdown set ai sw=2 sts=2 et
augroup END  
" ================

" Syntax highlighting and theme
syntax enable


" Configs to make Molokai look great
set background=dark  
let g:molokai_original=1  
let g:rehash256=1  


" Show trailing whitespace and spaces before a tab:
:highlight ExtraWhitespace ctermbg=red guibg=red
:autocmd Syntax * syn match ExtraWhitespace /\s\+$\| \+\ze\\t/

"searching
set hlsearch
set incsearch
set ignorecase
set smartcase


" Tab completion
set wildmode=list:longest,list:full  
set wildignore+=*.o,*.obj,.git,*.rbc,*.class,.svn,vendor/gems/*

"NERDTree abre y cierra
map <leader><F5> :NERDTreeToggle<CR>  
map <leader><F10> :GitGutterToggle<CR>  

"copiar y pegar con shift + ruedita del mouse o F9
set paste 

"no pegar 
set nopaste

set clipboard=unnamed 


"Refresh to NERDTree con los nuevos files, con Ctrl + l para refresh
fun! ToggleNERDTreeWithRefresh()
    :NERDTreeToggle 
    if(exists("b:NERDTreeType") == 1)
        call feedkeys("R")  
    endif   
endf 

nmap <silent> <c-l> :call ToggleNERDTreeWithRefresh()<cr> 


Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'jpo/vim-railscasts-theme'

"Plugin Emmet
Plugin 'mattn/emmet-vim'

"autoRefresh NERDTree para que no tenga que cerrar y abrir todo el tiempo
autocmd CursorHold,CursorHoldI * call NERDTreeFocus() | call g:NERDTree.ForCurrentTab().getRoot().refresh() | call g:NERDTree.ForCurrentTab().render() | wincmd w

"NERDTRee mostrame los archivos ocultos
let NERDTreeShowHidden=1

"Config de javascript
let g:javascript_plugin_jsdoc = 1
let g:javascript_plugin_flow = 1
"set foldmethod=syntax "metodo de plegado, 
